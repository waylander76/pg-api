# pg-api

## Description
This project is the API for the pg platform. Check [profile](https://gitlab.com/waylander76) for more info.

## Visuals
TODO - Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
TODO - Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
TODO - Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For any issue, requests or help, please open the issue describing your situation.

## Roadmap
TODO - For information about the whole ecosystem please refer to the [infrastructure]() project.

## License
TODO - For open source projects, say how it is licensed.
